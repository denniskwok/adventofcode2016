This is just a simple repository to hold my solutions to 2016's Advent of Code
and to learn Golang.  As of time of writing, I am a Go noob.

This should not be taken as an example of how normal
production code should be written; production code should have more comments,
context, runtime complexity optimizations and automated testing to verify the
solutions submitted are correct.