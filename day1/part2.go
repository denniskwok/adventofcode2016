package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strconv"
	"strings"
)

type Locations []Location

func (l Locations) Sort() {
	sort.Slice(l, func (i, j int) bool {
		lhs := l[i]
		rhs := l[j]

		return lhs.LessThan(rhs)
	})
}

func (l Locations) Contains(loc Location) (bool, int) {
	l.Sort()

	index := sort.Search(len(l), func(idx int) bool {
		return !l[idx].LessThan(loc)
	})

	if index < len(l) {
		// fmt.Println(l[index], loc)
	}

	if index < len(l) && l[index] == loc {
		return true, index
	}
	return false, -1
}

type Location struct {
	X int
	Y int
}

func (lhs Location) LessThan(rhs Location) bool {
	if lhs.X < rhs.X {
		return true
	} else if lhs.X > rhs.X {
		return false
	}
	return lhs.Y < rhs.Y
}

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func Move(previous *Locations, x *int, y *int, distance int, increment int, changeX bool) bool {
	change := y
	if changeX {
		change = x
	}

	for i := 0; i < distance; i++ {
		loc := Location{*x, *y}
		contains, _ := previous.Contains(loc)

		if contains {
			return true
		}

		*previous = append(*previous, loc)
		*change += increment
	}

	return false
}

func processMove(previous *Locations, move string, dir *string, x *int, y *int) bool {
	fmt.Print(move, " Start Orientation: ", *dir, " x: ", *x, " y: ", *y)

	turn := string(move[0])
	travel, err := strconv.Atoi(string(move[1:]))

	if err != nil {
		fmt.Println("Could not convert atoi", err)
		return false
	}

	done := false

	// process orientation first
	switch turn {
	case "L":
		switch *dir {
		case "N":
			*dir = "W"
			done = Move(previous, x, y, travel, -1, true)
		case "W":
			*dir = "S"
			done = Move(previous, x, y, travel, 1, false)
		case "S":
			*dir = "E"
			done = Move(previous, x, y, travel, 1, true)
		case "E":
			*dir = "N"
			done = Move(previous, x, y, travel, -1, false)
		}
	case "R":
		switch *dir {
		case "N":
			*dir = "E"
			done = Move(previous, x, y, travel, 1, true)
		case "W":
			*dir = "N"
			done = Move(previous, x, y, travel, -1, false)
		case "S":
			*dir = "W"
			done = Move(previous, x, y, travel, -1, true)
		case "E":
			*dir = "S"
			done = Move(previous, x, y, travel, 1, false)
		}
	}

	fmt.Println(" End x:", *x, "y:", *y)
	return done
}

func main() {
	previous := Locations{}
	input := readFile("./input.txt")
	pieces := strings.Split(input, ", ")

	dir := "N"
	x, y := 0, 0

	for _, p := range pieces {
		if processMove(&previous, strings.TrimSpace(p), &dir, &x, &y) {
			break
		}
	}

	fmt.Println("Final distance:", math.Abs(float64(x))+math.Abs(float64(y)))

}
