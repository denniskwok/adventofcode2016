package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func processMove(move string, dir *string, x *int, y *int) {
	fmt.Print(move, " Start Orientation: ", *dir, " x: ", *x, " y: ", *y)

	turn := string(move[0])
	travel, err := strconv.Atoi(string(move[1:]))

	if err != nil {
		fmt.Println("Could not convert atoi", err)
		return
	}

	// process orientation first
	switch turn {
	case "L":
		switch *dir {
		case "N":
			*dir = "W"
			*x -= travel
		case "W":
			*dir = "S"
			*y += travel
		case "S":
			*dir = "E"
			*x += travel
		case "E":
			*dir = "N"
			*y -= travel
		}
	case "R":
		switch *dir {
		case "N":
			*dir = "E"
			*x += travel
		case "W":
			*dir = "N"
			*y -= travel
		case "S":
			*dir = "W"
			*x -= travel
		case "E":
			*dir = "S"
			*y += travel
		}
	}

	fmt.Println(" End x:", *x, "y:", *y)
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, ", ")

	dir := "N"
	x, y := 0, 0

	for _, p := range pieces {
		processMove(strings.TrimSpace(p), &dir, &x, &y)
	}

	fmt.Println("Final distance:", math.Abs(float64(x))+math.Abs(float64(y)))

}
