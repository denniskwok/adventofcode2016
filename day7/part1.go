package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func sliceMatches(s string) bool {
	if len(s) < 4 {
		return false
	}

	return s[0] == s[3] && s[1] == s[2] && s[0] != s[1]
}

func scanLine(line string) bool {
	insideSquare := false
	oneMatch := false

	for i := 0; i < len(line)-3; i++ {
		if line[i] == '[' {
			insideSquare = true
			continue
		} else if line[i] == ']' {
			insideSquare = false
			continue
		}

		if insideSquare && sliceMatches(line[i:i+4]) {
			return false
		} else if !insideSquare && sliceMatches(line[i:i+4]) {
			oneMatch = true
		}
	}

	return oneMatch
}

func main() {
	input := readFile("./input.txt")
	// input := readFile("./example.txt")
	pieces := strings.Split(input, "\n")

	count := 0

	for _, p := range pieces {
		if scanLine(p) {
			count++
		}
	}

	fmt.Println(count)
}
