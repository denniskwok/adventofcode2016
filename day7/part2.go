package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func sliceIsOppositeOf(s string, s2 string) bool {
	return sliceMatches(s) && sliceMatches(s2) && s[0] == s2[1] && s[1] == s2[0]
}

func sliceMatches(s string) bool {
	if len(s) < 3 {
		return false
	}

	return s[0] == s[2] && s[0] != s[1]
}

func scanLine(line string) bool {
	insideSquare := false

	insideMatches := []string{}
	outsideMatches := []string{}

	for i := 0; i < len(line)-2; i++ {
		if line[i] == '[' {
			insideSquare = true
			continue
		} else if line[i] == ']' {
			insideSquare = false
			continue
		}

		piece := line[i : i+3]

		if insideSquare && sliceMatches(piece) {
			insideMatches = append(insideMatches, piece)
		} else if !insideSquare && sliceMatches(piece) {
			outsideMatches = append(outsideMatches, piece)
		}
	}

	// this could be more efficient as O(nlogn)
	for _, i := range insideMatches {
		for _, j := range outsideMatches {
			if sliceIsOppositeOf(i, j) {
				return true
			}
		}
	}

	return false
}

func main() {
	input := readFile("./input.txt")
	// input := readFile("./example.txt")
	pieces := strings.Split(input, "\n")

	count := 0

	for _, p := range pieces {
		if scanLine(p) {
			count++
		}
	}

	fmt.Println(count)
}
