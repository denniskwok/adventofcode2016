package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func addLine(line string, results *map[int]map[rune]int) {
	for i, r := range line {
		_, ok := (*results)[i]

		// make new heap
		if !ok {
			(*results)[i] = map[rune]int{}
		}

		// add rune count for this location
		(*results)[i][r]++
	}
}

func main() {
	input := readFile("./input.txt")
	// input := readFile("./example.txt")
	pieces := strings.Split(input, "\n")

	m := &map[int]map[rune]int{}

	for _, p := range pieces {
		addLine(p, m)
	}

	for i, val := range *m {
		largest := 0
		largestRune := ' '

		for r, c := range val {
			if c > largest {
				largestRune = r
				largest = c
			}
		}

		fmt.Printf("%d: %q\n", i, largestRune)
	}
}
