package main

import (
	"bytes"
	"container/heap"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type RuneFrequency struct {
	Frequency int
	Rune      rune
}

type MaxIntHeap []RuneFrequency

func (h MaxIntHeap) Len() int { return len(h) }
func (h MaxIntHeap) Less(i, j int) bool {
	if h[i].Frequency == h[j].Frequency {
		return h[i].Rune < h[j].Rune
	}
	return h[i].Frequency > h[j].Frequency
}
func (h MaxIntHeap) Swap(i, j int) { h[i], h[j] = h[j], h[i] }

func (h *MaxIntHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(RuneFrequency))
}

func (h *MaxIntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func split(line string) (string, int, string) {
	re, err := regexp.Compile(`(.*)-(\d+)\[(.*)\]`)

	if err != nil {
		fmt.Println(err)
		return "", 0, ""
	}

	matches := re.FindStringSubmatch(line)
	sector, err := strconv.Atoi(matches[2])

	if err != nil {
		fmt.Println(err)
		return "", 0, ""
	}

	return matches[1], sector, matches[3]
}

func calcChecksum(name string) string {
	m := map[rune]int{}

	for _, c := range name {
		if c != '-' {
			m[c]++
		}
	}

	h := &MaxIntHeap{}
	heap.Init(h)

	for k, v := range m {
		rf := RuneFrequency{v, k}
		heap.Push(h, rf)
	}
	var buffer bytes.Buffer
	for i := 0; i < 5; i++ {
		rf := heap.Pop(h).(RuneFrequency)
		buffer.WriteRune(rf.Rune)
	}

	return buffer.String()
}

func rotate(name string, rotate int) string {
	// shorten amount since it loops easily
	rotate %= 26

	var buffer bytes.Buffer

	for _, c := range name {
		if c == '-' {
			buffer.WriteRune(' ')
		} else {
			r := ((int(c) - int('a') + rotate) % 26) + int('a')
			buffer.WriteRune(rune(r))
		}
	}

	return buffer.String()
}

func process(line string) {
	// split string up by parts
	name, sector, checksum := split(line)

	if calcChecksum(name) == checksum {
		fmt.Println(sector, rotate(name, sector))
	}
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")

	for _, p := range pieces {
		process(p)
	}
}
