package main

import (
	"sort"
)

type StateCollection []State

func (sc StateCollection) SortStates() {
	// needSort := sort.SliceIsSorted(sc, func(i, j int) bool {
	// 	s1 := sc[i]
	// 	s2 := sc[j]
	// 	return s1.LessThan(s2)
	// })
	// 
	// if !needSort {
	// 	return
	// }

	sort.SliceStable(sc, func(i, j int) bool {
		s1 := sc[i]
		s2 := sc[j]
		return s1.LessThan(s2)
	})
}

func (sc StateCollection) Contains(s State) (bool, int) {
	sc.SortStates()

	index := sort.Search(len(sc), func(idx int) bool {
		return !sc[idx].LessThan(s)
	})

	if index != len(sc) && sc[index].SameState(s) {
		return true, index
	}
	return false, 0
}
