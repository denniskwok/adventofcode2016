package main

import (
	"testing"
)

func MakeTestFloor() Floor {
	items := make([]Item, 5)
	items[0].Type = Chip
	items[0].Element = "a"
	items[1].Type = RTG
	items[1].Element = "a"
	items[2].Type = Chip
	items[2].Element = "b"
	items[3].Type = RTG
	items[3].Element = "c"
	items[4].Type = Chip
	items[4].Element = "d"

	return items
}

func TestContains(t *testing.T) {
	f := MakeTestFloor()

	f.SortItems()

	contains, _ := f.Contains(Item{Chip, "a"})
	if !contains {
		t.Error("Failed to detect Chip a")
	}
}

func TestFloorAdd(t *testing.T) {
	f := Floor{}

	f.AddItem(Item{Chip, "a"})

	if len(f) != 1 {
		t.Error("Len is wrong")
		return
	}


	if f[0].Type != Chip || f[0].Element != "a" {
		t.Error("Contents wrong")
		return
	}
}

func TestFloorRemove(t *testing.T) {
	f := Floor{}

	f.AddItem(Item{Chip, "a"})
	f.AddItem(Item{RTG, "a"})

	if len(f) != 2 {
		t.Error("Len is wrong")
		return
	}

	f.RemoveItem(Item{Chip, "a"})

	if len(f) != 1 {
		t.Error("Len is wrong!")
		return
	}

	if f[0].Type != RTG || f[0].Element != "a" {
		t.Error("Contents are wrong!")
	}
}
