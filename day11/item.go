package main

import (
	"strings"
)

type ItemType int

const (
	Chip ItemType = iota
	RTG
)

type Item struct {
	Type    ItemType
	Element string
}

func (i Item) LessThan(i2 Item) bool {
	if i.Type == Chip && i2.Type == RTG {
		return true
	} else if i.Type == RTG && i2.Type == Chip {
		return false
	}

	return strings.Compare(i.Element, i2.Element) == -1
}
