package main

import (
	"sort"
)

type Floor []Item

func (f Floor) SortItems() {
	// needSort := sort.SliceIsSorted(f, func(i, j int) bool {
	//    lhs := f[i]
	//    rhs := f[j]
	//    return lhs.LessThan(rhs)
	// )

	// if !needSort {
	// 	return
	// }

	sort.SliceStable(f, func(i, j int) bool {
		lhs := f[i]
		rhs := f[j]
		return lhs.LessThan(rhs)
	})
}

func (f Floor) LessThan(f2 Floor) bool {
	f.SortItems()
	f2.SortItems()

	if len(f) < len(f2) {
		return true
	} else if len(f) > len(f2) {
		return false
	}

	for i := 0; i < len(f); i++ {
		if f[i].LessThan(f2[i]) {
			return true
		} else if f2[i].LessThan(f[i]) {
			return false
		}
	}
	return false
}

func (f Floor) Equal(f2 Floor) bool {
	if len(f) != len(f2) {
		return false
	}

	for i := 0; i < len(f); i++ {
		if f[i] != f2[i] {
			return false
		}
	}

	return true
}

func (f Floor) DeepCopy() Floor {
	f2 := make(Floor, 0)

	for i := 0; i < len(f); i++ {
		f2 = append(f2, f[i])
	}

	return f2
}

func (f Floor) IsBadState() bool {
	hasGenerators := false

	for _, i := range f {
		if i.Type == RTG {
			hasGenerators = true
			break
		}
	}

	for _, i := range f {
		// generators have no issues on their own.
		if i.Type == RTG {
			continue
		}

		// but if we have a chip without the generators we lose
		if hasGenerators && !f.MatchingGeneratorOnFloor(i) {
			return true
		}
	}

	return false
}

func (f *Floor) AddItem(i Item) {
	*f = append(*f, i)
}

func (f Floor) Contains(i Item) (bool, int) {
	f.SortItems()
	index := sort.Search(len(f), func(idx int) bool {
		return !f[idx].LessThan(i)
	})

	if index != len(f) && f[index] == i {
		return true, index
	}
	return false, 0
}

func (f *Floor) RemoveItem(i Item) {
	contains, index := f.Contains(i)
	if contains {
		if len(*f) == 1 {
			*f = Floor{}
			return
		}


		lastIndex := len(*f) - 1
		(*f)[index] = (*f)[lastIndex]
		*f = (*f)[0:lastIndex]
	}
}

func (f Floor) MatchingChipOnFloor(i Item) bool {
	i2 := Item{Chip, i.Element}
	contains, _ := f.Contains(i2)
	return contains
}

func (f Floor) MatchingGeneratorOnFloor(i Item) bool {
	i2 := Item{RTG, i.Element}
	contains, _ := f.Contains(i2)
	return contains
}
