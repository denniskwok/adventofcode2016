package main

import (
	"fmt"
	"testing"
)

func BuildStateCollection(numStates int) StateCollection {
	sc := StateCollection{}

	for i := 0; i < numStates; i++ {
		sc = append(sc, BuildState(i))
	}

	return sc
}

func BuildState(i int) State {
	s := State{}
	s.NumMoves = i
	s.Floors = make([]Floor, numFloors)
	s.ElevatorLocation = 0

	for j := 0; j < numFloors; j++ {
		s.Floors[j] = Floor{}
	}

	return s
}


func TestSortStates(t *testing.T) {
	sc := BuildStateCollection(5)
	sc.SortStates()
}

func TestStateCollectionContains(t *testing.T) {
	sc := BuildStateCollection(5)
	s := BuildState(1)

	contains, index := sc.Contains(s)
	if !contains {
		t.Error("Should have found state.")
		t.Error(s)
		t.Error(sc)
	}

	if index != 1 {
		t.Error(index)
	}
}

func TestStateCollectionContainsSameCount(t *testing.T) {
	sc := StateCollection{}
	s := BuildState(1)
	s.Floors[0] = append(s.Floors[0], Item{Chip, "hydrogen"})
	s.Floors[1] = append(s.Floors[1], Item{Chip, "lithium"})
	s.Floors[1] = append(s.Floors[1], Item{RTG, "hydrogen"})
	s.Floors[1] = append(s.Floors[1], Item{RTG, "lithium"})
	s.ElevatorLocation = 1

	s2 := BuildState(1)
	s2.Floors[1] = append(s2.Floors[1], Item{Chip, "hydrogen"})
	s2.Floors[0] = append(s2.Floors[0], Item{Chip, "lithium"})
	s2.Floors[1] = append(s2.Floors[1], Item{RTG, "hydrogen"})
	s2.Floors[1] = append(s2.Floors[1], Item{RTG, "lithium"})
	s2.ElevatorLocation = 1

	sc = append(sc, s)
	contains, index := sc.Contains(s2)

	fmt.Println(s)
	fmt.Println(s2)

	if contains {
		t.Error("Contains, when shouldn't :(")
		t.Error(index)
	}
}

