package main

import (
)

type State struct {
	ElevatorLocation int
	Floors	[]Floor
	NumMoves int
}

func (s State) LessThan(s2 State) bool {
	if s.NumMoves < s2.NumMoves {
		return true
	} else if s.NumMoves > s2.NumMoves {
		return false
	}

	if s.ElevatorLocation < s2.ElevatorLocation {
		return true
	} else if s.ElevatorLocation > s2.ElevatorLocation {
		return false
	}

	for i := 0; i < numFloors; i++ {
		if s.Floors[i].LessThan(s2.Floors[i]) {
			return true
		} else if s2.Floors[i].LessThan(s.Floors[i]) {
			return false
		}
	}

	return false
}

func (s State) SameState(s2 State) bool {
	if s.ElevatorLocation != s2.ElevatorLocation {
		return false
	}

	for i := 0; i < numFloors; i++ {
		s.Floors[i].SortItems()
		s2.Floors[i].SortItems()

		if !s.Floors[i].Equal(s2.Floors[i]) {
			return false
		}
	}

	return true
}

func (s State) DeepCopy() State {
	s2 := State{}
	s2.ElevatorLocation = s.ElevatorLocation
	s2.Floors = make([]Floor, numFloors)

	for i := 0; i < numFloors; i++ {
		s2.Floors[i] = s.Floors[i].DeepCopy()
	}

	s2.NumMoves = s.NumMoves
	return s2
}

func (s State) IsBadState() bool {
	for _, f := range s.Floors {
		if f.IsBadState() {
			return true
		}
	}
	return false
}

func (s State) IsFinished() bool {
	return len(s.Floors[targetFloor-1]) == numItems
}

