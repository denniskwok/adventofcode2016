package main

import (
	"fmt"
)

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

const (
	maxExecutions = 10000000
	numFloors     = 4
	numItems      = 14
	printEvery    = 100
	targetFloor   = 4
)

func main() {
	checkedStates := StateCollection{}
	stateQueue := StateCollection{}
	state := State{}
	state.ElevatorLocation = 0
	state.Floors = make([]Floor, numFloors)
	state.NumMoves = 0

	// state.Floors[0] = append(state.Floors[0], Item{Chip, "hydrogen"})
	// state.Floors[0] = append(state.Floors[0], Item{Chip, "lithium"})
	// state.Floors[1] = append(state.Floors[1], Item{RTG, "hydrogen"})
	// state.Floors[1] = append(state.Floors[1], Item{RTG, "lithium"})

	// first floor populate
	state.Floors[0] = append(state.Floors[0], Item{RTG, "polonium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "thulium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "thulium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "promethium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "ruthenium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "ruthenium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "cobalt"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "cobalt"})

	state.Floors[0] = append(state.Floors[0], Item{RTG, "elerium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "elerium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "dilithium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "dilithium"})

	// second floor populate
	state.Floors[1] = append(state.Floors[1], Item{Chip, "polonium"})
	state.Floors[1] = append(state.Floors[1], Item{Chip, "promethium"})

	// add initial state
	stateQueue = append(stateQueue, state)
	executions := 0

	for len(stateQueue) > 0 {
		// pop item off
		workingState := stateQueue[0]
		stateQueue = stateQueue[1:]

		executions++

		if executions%printEvery == 0 {
			fmt.Printf("sq: %d %v\n", len(stateQueue), workingState)
		}

		// if this breaks things for some reason
		if workingState.IsBadState() {
			fmt.Println("State is bad, skipping.")
			continue
		}

		// if this is finished, then we're done!  yay
		if workingState.IsFinished() {
			fmt.Println(workingState.NumMoves)
			fmt.Printf("%v\n", workingState)
			break
		}

		if workingState.NumMoves >= maxExecutions {
			fmt.Println("Recursed too deep.")
			break
		}

		contains, _ := checkedStates.Contains(workingState)
		if contains {
			continue
		}

		checkedStates = append(checkedStates, workingState)
		adjacentFloors := make([]int, 0)

		if workingState.ElevatorLocation > 0 {
			adjacentFloors = append(adjacentFloors, workingState.ElevatorLocation-1)
		}

		if workingState.ElevatorLocation < numFloors-1 {
			adjacentFloors = append(adjacentFloors, workingState.ElevatorLocation+1)
		}

		items := workingState.Floors[workingState.ElevatorLocation]
		// otherwise, iterate! start with single item moves only
		for _, i := range items {
			for _, j := range adjacentFloors {
				s := workingState.DeepCopy()
				s.Floors[workingState.ElevatorLocation].RemoveItem(i)

				hasI, _ := s.Floors[j].Contains(i)
				if hasI {
					fmt.Println("Weird this floor already has this item")
					continue
				}

				s.Floors[j].AddItem(i)
				s.ElevatorLocation = j
				s.NumMoves++

				// fmt.Println(i, j)
				// fmt.Println(s)
				// fmt.Println(workingState)
				// fmt.Println()

				// add to state queue
				inCheckedStates, _ := checkedStates.Contains(s)
				inStateQueue, _ := stateQueue.Contains(s)

				// fmt.Printf("Checking %v\n", s)
				// fmt.Println(stateQueue)
				// fmt.Println(s.IsBadState(), inCheckedStates, inStateQueue)

				if !s.IsBadState() && !inCheckedStates && !inStateQueue {
					// if !s.IsBadState() && !StateInSlice(s, checkedStates) {
					// if !s.IsBadState() {
					stateQueue = append(stateQueue, s)
				}
			}
		}

		// now do double items
		for _, i := range items {
			for _, j := range items {
				if i == j {
					continue
				}

				for _, k := range adjacentFloors {
					s := workingState.DeepCopy()
					s.Floors[workingState.ElevatorLocation].RemoveItem(i)
					s.Floors[workingState.ElevatorLocation].RemoveItem(j)

					hasI, _ := s.Floors[k].Contains(i)
					hasJ, _ := s.Floors[k].Contains(j)

					if hasI || hasJ {
						fmt.Println("Weird, this floor already contains this item.")
						continue
					}

					s.Floors[k].AddItem(i)
					s.Floors[k].AddItem(j)
					s.ElevatorLocation = k
					s.NumMoves++

					inCheckedStates, _ := checkedStates.Contains(s)
					inStateQueue, _ := stateQueue.Contains(s)

					// add to state queue
					if !s.IsBadState() && !inCheckedStates && !inStateQueue {
						// if !s.IsBadState() && !StateInSlice(s, checkedStates) {
						// if !s.IsBadState() {
						stateQueue = append(stateQueue, s)
					}
				}
			}
		}
		stateQueue.SortStates()
	}
	fmt.Println("end")
}
