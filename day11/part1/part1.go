package main

import (
	"fmt"
	"sort"
)

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

type ItemType int

const (
	maxExecutions = 1000000
	numFloors   = 4
	numItems    = 10
	printEvery  = 100
	targetFloor = 4
)

const (
	Chip ItemType = iota
	RTG
)

type State struct {
	ElevatorLocation int
	Floors           []Floor
	NumMoves         int
}

func (s State) Equal(s2 State) bool {
	if s.ElevatorLocation != s2.ElevatorLocation {
		return false
	}

	for i := 0; i < numFloors; i++ {
		if !s.Floors[i].Equal(s2.Floors[i]) {
			return false
		}
	}
	return true
}

func (s State) DeepCopy() State {
	s2 := State{}
	s2.ElevatorLocation = s.ElevatorLocation
	s2.Floors = make([]Floor, numFloors)
	for i := 0; i < numFloors; i++ {
		s2.Floors[i] = make(Floor, len(s.Floors[i]))
		copy(s2.Floors[i], s.Floors[i])
	}
	s2.NumMoves = s.NumMoves
	return s2
}

// a state is bad if a chip is not connected to its own RTG
// and another RTG is on the same floor
func (s State) IsBadState() bool {
	for _, f := range s.Floors {
		if f.IsBadState() {
			return true
		}
	}
	return false
}

// we are finished if all of the items are on the 4th floor
func (s State) IsFinished() bool {
	return len(s.Floors[targetFloor-1]) == numItems
}

type Floor []Item

// for sort
func (f Floor) Len() int { return len(f) }
func (f Floor) Swap(i, j int) { f[i], f[j] = f[j], f[i]}
func (f Floor) Less(i, j int) bool {
	if f[i].Element == f[j].Element {
		return f[i].Type == Chip
	}

	return f[i].Element < f[j].Element
}

func (f Floor) Equal(f2 Floor) bool {
	if len(f) != len(f2) {
		return false
	}

	sort.Sort(f)
	sort.Sort(f2)
	
	for i := 0; i < len(f); i++ {
		if f[i] != f2[i] {
			return false
		}
	}
	return true
}

func (f Floor) IsBadState() bool {
	hasGenerators := false
	for _, i := range f {
		if i.Type == RTG {
			hasGenerators = true
			break
		}
	}

	for _, i := range f {
		// generators don't have issues
		if i.Type == RTG {
			continue
		}

		// but chips do, so if the matching generator isn't here we're broken.
		if hasGenerators && !i.MatchingGeneratorOnFloor(f) {
			return true
		}
	}
	return false
}

func (f *Floor) AddItem(i Item) {
	*f = append(*f, i)
}

func (f *Floor) RemoveItem(i Item) {
	for idx, v := range *f {
		if i == v {
			// fmt.Println(i, v, f)
			lastIndex := len(*f) - 1
			// swap item to drop to end
			(*f)[idx] = (*f)[lastIndex]
			// then slice off
			*f = (*f)[:lastIndex]
		}
	}
}

type Item struct {
	Type    ItemType
	Element string
}

func (i Item) MatchingChipOnFloor(f Floor) bool {
	i.Type = Chip
	return i.IsOnFloor(f)
}

func (i Item) MatchingGeneratorOnFloor(f Floor) bool {
	i.Type = RTG
	return i.IsOnFloor(f)
}

func (i Item) IsOnFloor(f Floor) bool {
	for _, floorItem := range f {
		if i == floorItem {
			return true
		}
	}
	return false
}

func StateInSlice(s State, l []State) bool {
	for _, s2 := range l {
		if s.Equal(s2) {
			return true
		}
	}
	return false
}

func main() {
	checkedStates := make([]State, 0)
	stateQueue := make([]State, 0)
	state := State{}
	state.ElevatorLocation = 0
	state.Floors = make([]Floor, numFloors)
	state.NumMoves = 0

	// state.Floors[0] = append(state.Floors[0], Item{Chip, "hydrogen"})
	// state.Floors[0] = append(state.Floors[0], Item{Chip, "lithium"})
	// state.Floors[1] = append(state.Floors[1], Item{RTG, "hydrogen"})
	// state.Floors[1] = append(state.Floors[1], Item{RTG, "lithium"})

	// first floor populate
	state.Floors[0] = append(state.Floors[0], Item{RTG, "polonium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "thulium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "thulium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "promethium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "ruthenium"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "ruthenium"})
	state.Floors[0] = append(state.Floors[0], Item{RTG, "cobalt"})
	state.Floors[0] = append(state.Floors[0], Item{Chip, "cobalt"})

	// second floor populate
	state.Floors[1] = append(state.Floors[1], Item{Chip, "polonium"})
	state.Floors[1] = append(state.Floors[1], Item{Chip, "promethium"})

	// add initial state
	stateQueue = append(stateQueue, state)
	executions := 0

	for len(stateQueue) > 0 {
		// pop item off
		workingState := stateQueue[0]
		stateQueue = stateQueue[1:]

		executions++

		if executions % printEvery == 0 {
			fmt.Printf("%v\n", workingState)
		}

		// if this breaks things for some reason
		if workingState.IsBadState() {
			fmt.Println("State is bad, skipping.")
			continue
		}

		// if this is finished, then we're done!  yay
		if workingState.IsFinished() {
			fmt.Println(workingState.NumMoves)
			fmt.Printf("%v\n", workingState)
			break
		}

		if workingState.NumMoves >= maxExecutions {
			fmt.Println("Recursed too deep.")
			break
		}

		if StateInSlice(workingState, checkedStates) {
			continue
		}

		checkedStates = append(checkedStates, workingState)
		adjacentFloors := make([]int, 0)

		if workingState.ElevatorLocation > 0 {
			adjacentFloors = append(adjacentFloors, workingState.ElevatorLocation-1)
		}

		if workingState.ElevatorLocation < numFloors - 1 {
			adjacentFloors = append(adjacentFloors, workingState.ElevatorLocation+1)
		}

		items := workingState.Floors[workingState.ElevatorLocation]
		// otherwise, iterate! start with single item moves only
		for _, i := range items {
			for _, j := range adjacentFloors {

				s := workingState.DeepCopy()
				s.Floors[workingState.ElevatorLocation].RemoveItem(i)
				s.Floors[j].AddItem(i)
				s.ElevatorLocation = j
				s.NumMoves++

				// add to state queue
				if !s.IsBadState() && !StateInSlice(s, checkedStates) && !StateInSlice(s, stateQueue) {
				// if !s.IsBadState() && !StateInSlice(s, checkedStates) {
				// if !s.IsBadState() {
					stateQueue = append(stateQueue, s)
				}
			}
		}

		// now do double items
		for _, i := range items {
			for _, j := range items {
				if i == j {
					continue
				}

				for _, k := range adjacentFloors {

					s := workingState.DeepCopy()
					s.Floors[workingState.ElevatorLocation].RemoveItem(i)
					s.Floors[workingState.ElevatorLocation].RemoveItem(j)
					s.Floors[k].AddItem(i)
					s.Floors[k].AddItem(j)
					s.ElevatorLocation = k
					s.NumMoves++

					// add to state queue
					if !s.IsBadState() && !StateInSlice(s, checkedStates) && !StateInSlice(s, stateQueue) {
					// if !s.IsBadState() && !StateInSlice(s, checkedStates) {
					// if !s.IsBadState() {
						stateQueue = append(stateQueue, s)
					}
				}
			}
		}
	}
	fmt.Println("end")
}
