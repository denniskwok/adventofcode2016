package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func minMax(lhs, rhs int) (int, int) {
	if lhs < rhs {
		return lhs, rhs
	}
	return rhs, lhs
}

type DestinationType int

const (
	Output DestinationType = iota
	OtherBot
)

type Destination struct {
	Number int
	Kind   DestinationType
}

func makeDestination(kind string, number string) (Destination, error) {
	d := Destination{}

	num, err := strconv.Atoi(number)

	if err != nil {
		fmt.Println(err)
		return d, err
	}

	d.Number = num

	switch kind {
	case "bot":
		d.Kind = OtherBot
	case "output":
		d.Kind = Output
	default:
		return d, errors.New("Invalid destination!")
	}

	return d, nil
}

type Bots []Bot

func (a Bots) Len() int           { return len(a) }
func (a Bots) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a Bots) Less(i, j int) bool { return a[i].Number < a[j].Number }

func CreateOrReturn(botNumber int, bots *Bots) *Bot {
	sort.Sort(*bots)

	i := sort.Search(len(*bots), func(i int) bool { return (*bots)[i].Number >= botNumber })
	if i < len(*bots) && (*bots)[i].Number == botNumber {
		return &(*bots)[i]
	} else {
		b := Bot{}
		b.Number = botNumber
		*bots = append(*bots, b)
		return &((*bots)[len(*bots)-1])
	}
}

type Bot struct {
	Number          int
	LowDestination  Destination
	HighDestination Destination
	HeldValue       int
}

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func processRuleSet(targetBotNumber, lowDestType, lowDestNumber, highDestType, highDestNumber string, bots *Bots) {
	lowDest, err := makeDestination(lowDestType, lowDestNumber)

	if err != nil {
		fmt.Println(err)
		return
	}

	highDest, err := makeDestination(highDestType, highDestNumber)

	if err != nil {
		fmt.Println(err)
		return
	}

	botNumber, err := strconv.Atoi(targetBotNumber)

	if err != nil {
		fmt.Println(err)
		return
	}

	b := CreateOrReturn(botNumber, bots)
	(*b).LowDestination = lowDest
	(*b).HighDestination = highDest
}

func buildRuleSet(line string, bots *Bots) {
	ruleSet, err := regexp.Compile(`bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)`)

	if err != nil {
		fmt.Println(err)
		return
	}

	results := ruleSet.FindStringSubmatch(line)

	if len(results) != 0 {
		processRuleSet(results[1], results[2], results[3], results[4], results[5], bots)
		return
	}
}

func giveBotNumber(botNumber int, value int, bots *Bots) {
	b := CreateOrReturn(botNumber, bots)

	// only has one item
	if b.HeldValue == 0 {
		b.HeldValue = value
		return
	}

	// now we have two, need to figure out which goes where.
	low, high := minMax(b.HeldValue, value)
	// let go of existing held value
	b.HeldValue = 0

	processDestination(b.LowDestination, low, bots)
	processDestination(b.HighDestination, high, bots)
}

func processDestination(d Destination, value int, bots *Bots) {
	if d.Kind == Output {
		fmt.Printf("Output %d receives %d\n", d.Number, value)
	} else {
		giveBotNumber(d.Number, value, bots)
	}
}

func processValueGiven(value, targetBotNumber string, bots *Bots) {
	valueGiven, err := strconv.Atoi(value)

	if err != nil {
		fmt.Println(err)
		return
	}

	botNumber, err := strconv.Atoi(targetBotNumber)

	if err != nil {
		fmt.Println(err)
	}

	giveBotNumber(botNumber, valueGiven, bots)
}

func buildValueSets(line string, bots *Bots) {
	valueGive, err := regexp.Compile(`value (\d+) goes to bot (\d+)`)

	if err != nil {
		fmt.Println(err)
		return
	}

	results := valueGive.FindStringSubmatch(line)

	if len(results) != 0 {
		processValueGiven(results[1], results[2], bots)
		return
	}
}

func main() {
	input := readFile("./input.txt")
	// input := readFile("./example.txt")
	pieces := strings.Split(input, "\n")
	bots := Bots{}

	for _, p := range pieces {
		buildRuleSet(p, &bots)
	}

	for _, p := range pieces {
		buildValueSets(p, &bots)
	}
}
