package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func processLine(line string) {
	repeatParser, _ := regexp.Compile(`(\d+)x(\d+)`)

	line = strings.Replace(line, " ", "", -1)
	buffer := bytes.NewBufferString("")

	for i := 0; i < len(line); i++ {
		if line[i] == '(' {
			endIndex := strings.Index(line[i:len(line)], ")")
			repeatString := line[i+1 : endIndex+i]

			results := repeatParser.FindStringSubmatch(repeatString)

			readAmount, _ := strconv.Atoi(results[1])
			repeatCount, _ := strconv.Atoi(results[2])

			i = endIndex + 1 + i

			repeatContent := line[i : i+readAmount]

			for j := 0; j < repeatCount; j++ {
				buffer.WriteString(repeatContent)
			}
			i = i + readAmount - 1
		} else {
			buffer.WriteByte(line[i])
		}
	}

	fmt.Println(buffer.Len())
	fmt.Println(buffer.String())
}

func main() {
	input := readFile("./input.txt")
	// input := readFile("./example.txt")
	pieces := strings.Split(input, "\n")

	for _, p := range pieces {
		processLine(p)
	}
}
