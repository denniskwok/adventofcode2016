package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func getLengths(line string) (int, int, int) {
	var i, j, k int

	fmt.Sscanf(line, "%d %d %d", &i, &j, &k)

	return i, j, k
}

func isValidTriangle(i int, j int, k int) bool {
	return (i+j) > k && (j+k) > i && (i+k) > j
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")
	count := 0

	for i := 0; i+2 < len(pieces); i += 3 {
		u1, v1, w1 := getLengths(pieces[i])
		u2, v2, w2 := getLengths(pieces[i+1])
		u3, v3, w3 := getLengths(pieces[i+2])

		if isValidTriangle(u1, u2, u3) {
			count++
		}

		if isValidTriangle(v1, v2, v3) {
			count++
		}

		if isValidTriangle(w1, w2, w3) {
			count++
		}
	}

	fmt.Println(count)
}
