package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func isTriangle(line string) bool {
	var i, j, k int

	if line == "" {
		return false
	}

	fmt.Sscanf(line, "%d %d %d", &i, &j, &k)

	return (i+j) > k && (j+k) > i && (i+k) > j
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")
	count := 0

	for _, p := range pieces {
		if isTriangle(p) {
			count++
		}
	}

	fmt.Println(count)
}
