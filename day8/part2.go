package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

const gridWidth = 50
const gridHeight = 6

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func processRect(x string, y string, grid *[gridHeight][gridWidth]bool) {
	xInt, _ := strconv.Atoi(x)
	yInt, _ := strconv.Atoi(y)

	for i := 0; i < xInt; i++ {
		for j := 0; j < yInt; j++ {
			grid[j][i] = true
		}
	}
}

func processRotateRow(rowNumber string, amount string, grid *[gridHeight][gridWidth]bool) {
	fmt.Println(rowNumber, amount)
	row, _ := strconv.Atoi(rowNumber)
	amt, _ := strconv.Atoi(amount)
	amt %= gridWidth

	oldRow := [gridWidth]bool{}
	for i := 0; i < gridWidth; i++ {
		oldRow[i] = grid[row][i]
	}

	for i := 0; i < gridWidth; i++ {
		grid[row][i] = oldRow[(gridWidth+i-amt)%gridWidth]
	}
}

func processRotateColumn(colNumber string, amount string, grid *[gridHeight][gridWidth]bool) {
	fmt.Println(colNumber, amount)
	col, _ := strconv.Atoi(colNumber)
	amt, _ := strconv.Atoi(amount)
	amt %= gridHeight

	oldColumn := [gridHeight]bool{}
	for i := 0; i < gridHeight; i++ {
		oldColumn[i] = grid[i][col]
	}

	for i := 0; i < gridHeight; i++ {
		grid[i][col] = oldColumn[(gridHeight+i-amt)%gridHeight]
	}
}

func processCommand(line string, grid *[gridHeight][gridWidth]bool) {
	rect, err := regexp.Compile(`rect (\d+)x(\d+)`)
	rotate, err := regexp.Compile(`rotate \w* (x|y)=(\d+) by (\d+)`)

	if err != nil {
		fmt.Println(err)
		return
	}

	results := rect.FindStringSubmatch(line)
	if len(results) != 0 {
		fmt.Println(results[0])
		processRect(results[1], results[2], grid)
	}

	results = rotate.FindStringSubmatch(line)
	if len(results) != 0 {
		fmt.Println(results[0])
		if results[1] == "y" {
			processRotateRow(results[2], results[3], grid)
		} else if results[1] == "x" {
			processRotateColumn(results[2], results[3], grid)
		}
	}
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")

	grid := [gridHeight][gridWidth]bool{}

	for _, p := range pieces {
		processCommand(p, &grid)
	}

	count := 0
	for j := 0; j < gridHeight; j++ {
		for i := 0; i < gridWidth; i++ {
			if grid[j][i] {
				fmt.Print("*")
				count++
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println("")
	}

	fmt.Println(count)
}
