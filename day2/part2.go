package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func canGoLeft(x int, y int) bool {
	if y == 1 || y == 3 {
		return x > 1
	} else if y == 2 {
		return x > 0
	} else {
		return false
	}
}

func canGoUp(x int, y int) bool {
	if x == 1 || x == 3 {
		return y > 1
	} else if x == 2 {
		return y > 0
	} else {
		return false
	}
}

func canGoRight(x int, y int) bool {
	if y == 1 || y == 3 {
		return x < 3
	} else if y == 2 {
		return x < 4
	} else {
		return false
	}
}

func canGoDown(x int, y int) bool {
	if x == 1 || x == 3 {
		return y < 3
	} else if x == 2 {
		return y < 4
	} else {
		return false
	}
}

func doMove(x int, y int, b rune) (int, int) {
	switch b {
	case 'U':
		if canGoUp(x, y) {
			y--
		}
	case 'D':
		if canGoDown(x, y) {
			y++
		}
	case 'L':
		if canGoLeft(x, y) {
			x--
		}
	case 'R':
		if canGoRight(x, y) {
			x++
		}
	}

	return x, y
}

func processLine(x int, y int, move string) (int, int) {
	for _, c := range move {
		x, y = doMove(x, y, c)
	}

	return x, y
}

func printKey(x int, y int) {
	switch y {
	case 0:
		fmt.Println("1")
	case 1:
		val := x + 1
		fmt.Println(val)
	case 2:
		val := x + 5
		fmt.Println(val)
	case 3:
		val := 'A' + x - 1
		fmt.Printf("%q\n", val)
	case 4:
		fmt.Println("D")
	}
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")

	x := 1
	y := 1

	for _, p := range pieces {
		if p == "" {
			continue
		}

		x, y = processLine(x, y, p)
		printKey(x, y)
	}
}
