package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func readFile(name string) string {
	payload, err := ioutil.ReadFile(name)

	if err != nil {
		fmt.Println("Could not open file!", err)
	}

	return string(payload)
}

func doMove(x int, y int, b rune) (int, int) {
	switch b {
	case 'U':
		y--
	case 'D':
		y++
	case 'L':
		x--
	case 'R':
		x++
	}

	if y < 0 {
		y = 0
	}

	if x < 0 {
		x = 0
	}

	if y > 2 {
		y = 2
	}

	if x > 2 {
		x = 2
	}

	return x, y
}

func processLine(x int, y int, move string) (int, int) {
	for _, c := range move {
		x, y = doMove(x, y, c)
	}

	return x, y
}

func printKey(x int, y int) {
	val := (x + 1) + (y * 3)
	fmt.Println(val)
}

func main() {
	input := readFile("./input.txt")
	pieces := strings.Split(input, "\n")

	x := 1
	y := 1

	for _, p := range pieces {
		if p == "" {
			continue
		}

		x, y = processLine(x, y, p)
		printKey(x, y)
	}
}
