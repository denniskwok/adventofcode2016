package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strconv"
)

func printIfInteresting(prefix string, num int, ignore []int) (bool, int) {
	var hash string
	h := md5.New()
	io.WriteString(h, prefix)
	io.WriteString(h, strconv.Itoa(num))
	hash = fmt.Sprintf("%x", h.Sum(nil))

	// fmt.Println(hash)

	if hash[0:5] == "00000" {
		position := int(hash[5]) - int('0')

		if position < 8 {

			for _, v := range ignore {
				if position == v {
					return false, -1
				}
			}
			fmt.Printf("Pos: %d Val: %q\n", position, hash[6])
			return true, position
		}
	}
	return false, -1
}

func main() {
	payload := "wtnhxymk"

	// printIfInteresting("abc", 3231929)
	// printIfInteresting("abc", 5017308)
	// printIfInteresting("abc", 5357525)

	wins := 0
	i := 0
	spotsFilled := []int{}

	for len(spotsFilled) < 8 {
		won, position := printIfInteresting(payload, i, spotsFilled)

		if won {
			wins++
			spotsFilled = append(spotsFilled, position)
		}
		i++
	}

	fmt.Println("")
}
