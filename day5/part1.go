package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strconv"
)

func printIfInteresting(prefix string, num int) bool {
	var hash string
	h := md5.New()
	io.WriteString(h, prefix)
	io.WriteString(h, strconv.Itoa(num))
	hash = fmt.Sprintf("%x", h.Sum(nil))

	// fmt.Println(hash)

	if hash[0:5] == "00000" {
		fmt.Printf("%q", hash[5])
		return true
	}
	return false
}

func main() {
	payload := "wtnhxymk"

	// printIfInteresting("abc", 3231929)

	wins := 0
	i := 0
	for wins < 8 {
		if printIfInteresting(payload, i) {
			wins++
		}
		i++
	}

	fmt.Println("")
}
